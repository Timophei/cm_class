#include <iostream>
#include <functional>
#include <iomanip>
#include <cmath>

using namespace std;

//---Func for integrate-------------------------------------------------
double f4a(double x)
{
    return 1.0/(1.0+pow(x,2.0));
}

double f4b(double x)
{
    return pow(x,1.0/3.0)*exp(sin(x));
}

double f5a(double x)
{
    return (1.0+cos(x))/(pow(x,1.0/3.0));
}

double f5b(double t) // var changed!
{
    double x = t/(1-t);
    return (1.0+exp(-x))/(1.0+pow(x,3.0/2.0))/((1-t)*(1-t));
}

//---Integral Func------------------------------------------------------
double IntegrateTrapezConstStep(const function<double(double)> & f, double a, double b, int N)
{
    double dx = (b-a)/double(N);
    double I = 0.0;
    
    for (int i = 0; i < N; i++)
    {
        double xU = a + dx*double(i+1);
        double xD = a + dx*double(i);
        I += (f(xD)+f(xU))*0.5*dx;
    }
    
    return I;
}

double IntegrateSimpsonConstStep(const function<double(double)> & f, double a, double b, int N)
{
    double dx = (b-a)/double(N);
    double I = 0.0;
    
    for (int i = 0; i < N; i++)
    {
        double xU = a + dx*double(i+1);
        double xD = a + dx*double(i);
        double xM = 0.5*(xU+xD);
        I += (f(xD)+4.0*f(xM)+f(xU))*dx/6.0;
    }
    
    return I;
}

double IntegrateSimpsonVarChange(const function<double(double)> & f, double a, double b, int N)
{
    double I = 0.0;
    
    if (isinf(f(a)) || isinf(f(b)) || isnan(f(a)) || isnan(f(b)))
    {
        double aT = -M_PI*double(N-1)/(sqrt(2*double(N)));
        double bT = M_PI*double(N-1)/(sqrt(2*double(N)));
        I = IntegrateSimpsonConstStep
        (
            [&] (double t)
            {
                double x = 0.5*((a+b) + (b-a)*tanh(t));
                return 0.5*f(x)*(b-a)/(cosh(t)*cosh(t));
            },
            aT,
            bT,
            N
        );
    }
    else
    {
        I = IntegrateSimpsonConstStep(f,a,b,N);
    }

    return I;
}
//----------------------------------------------------------------------
int main ()
{
    int N = 128;
    double f4a_value = 1.5707963267949;
    double f4b_value = 1.2958740087317;
    double f5a_value = 2.8212230741459;
    double f5b_value = 3.03147;
    //-- task 4 ---
    cout << "\n f4a trapez" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateTrapezConstStep(f4a,-1.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f4a_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f4a_value)/f4a_value << " %" << endl;
    }
    cout << "\n f4a simpson" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateSimpsonConstStep(f4a,-1.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f4a_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f4a_value)/f4a_value << " %" << endl;
    }
    cout << "\n f4b trapez" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateTrapezConstStep(f4b,0.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f4b_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f4b_value)/f4b_value << " %" << endl;
    }
    cout << "\n f4b simpson" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateSimpsonConstStep(f4b,0.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f4b_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f4b_value)/f4b_value << " %" << endl;
    }
    //-- task 5 --
    cout << "\n f5a simpson" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateSimpsonVarChange(f5a,0.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f5a_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f5a_value)/f5a_value << " %" << endl;
    }
    cout << "\nf5b simpson" << endl;
    for (int n = 4; n <= N; n = n*2)
    {
        double I = IntegrateSimpsonVarChange(f5b,0.0,1.0,n);
        cout << setprecision(15)
             << "N = " << n << "\t  I = " << I << "\t err = " << fabs(I - f5b_value) << " - " 
             << setprecision(3) << 100.0*fabs(I - f5b_value)/f5b_value << " %" << endl;
    }
    cout << "DOVEN DOVEN DOVEN" << endl;
    return 0;
}

