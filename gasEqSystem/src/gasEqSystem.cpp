/**********************************************************************\
 * 
 * class gasEqSystem implementation
 * 
 * Description
 *      class for system of first order ordinary differential equation
 *      Ut + Fx = R
 * 
 * HeaderFiles
 *      gasEqSystem.H
 * 
\**********************************************************************/
#include "gasEqSystem.H"

using namespace std;

/**********************************************************************\
 * low level fuction
\**********************************************************************/
int gasEqSystem::index(int i)
{
    return i + 3;
}

Vector gasEqSystem::F(int i)
{
    Vector ui = U_[i];
    Vector f(var::N);
    f[var::rho] = ui[var::rho];//ui[var::m];
    //double v = ui[var::m]/ui[var::rho]/A(i);
    //f[var::m] = ui[var::m]*v + p[i];
    //f[var::e] = (ui[var::e] + p[i]/ui[var::rho])*v;
    return f;
}

Vector gasEqSystem::R(int i)
{
    Vector f(var::N);
    f[var::rho] = 0.0;
    //f[var::m] = -p[i]*2.*(1-2.*dx_*i);
    //f[var::e] = 0.0;
    return f;
}

double gasEqSystem::A(int i)
{
    return 1;//nozzleShape_(dx_*i);
}

/**********************************************************************\
 * low level WENO fuction
\**********************************************************************/
double gasEqSystem::WENO_Cri(int r, int i)
{
    return WENO_CRI[3*r+i];
}

Vector gasEqSystem::WENO_flux(int j)
{
    Vector f(var::N);
    
    std::vector<Vector> Ur(WENO_K);
    std::vector<Vector> w(WENO_K);
    std::vector<Vector> IS(WENO_K);
    std::vector<Vector> sigma(WENO_K);
    Vector sigmaSum(var::N);
    
    for (int r = 0; r < WENO_K; r++)
    {
        Vector tUr(var::N);
        for(int i = 0; i < WENO_K; i++)
        {
            //if (j == 2 && r == 2) cout << j << " - " << r << " - " << i << " " << WENO_Cri(r,i) <<endl << F(j-r+i) << WENO_Cri(r,i)*F(j-r+i);
            
            tUr = tUr + WENO_Cri(r,i)*F(j-r+i);
        }
        Ur[r] = tUr;
    }
    
    IS[0] = 13.0*sq(F(j) - 2.0*F(j+1) + F(j+2))/12.0 + sq(3.0*F(j) - 4.0*F(j+1) + F(j+2))/4.0;
    IS[1] = 13.0*sq(F(j-1) -2.0*F(j) + F(j+1))/12.0 + sq(F(j-1) - F(j+1))/4.0;
    IS[2] = 13.0*sq(F(j-2) -2.0*F(j-1) + F(j))/12.0 + sq(F(j-2) - 4.0*F(j-1) + 3.0*F(j))/4.0;
    
    //if (j == 2) cout << "IS " << endl << IS[0] << IS[1] << IS[2] << endl;
    //if (j > 0 && j < 6) cout << ;
    
    
    for (int i = 0; i < WENO_K; i++)
    {
        sigma[i] = WENO_OMEGA[i]/sq(WENO_EPSILON + IS[i]);
        sigmaSum = sigmaSum + sigma[i];
    }
    
    for (int i = 0; i < WENO_K; i++)
    {
        w[i] = sigma[i]/sigmaSum;
    }

    for(int i = 0; i < WENO_K; i++)
    {
        //if (j < 4) cout << j << "-"<< i << endl <<"w = " << w[i] << " U = " << Ur[i] << endl;
        f = f + w[i]*Ur[i];
    }
    //if (j > 0 && j < 4) cout << f << endl;
    //cout << "f(" << j << ") = "  << f << endl;
    
    return f;
}
/**********************************************************************\
 * constructor
\**********************************************************************/
gasEqSystem::gasEqSystem(const po::variables_map &vm)
{
    cout << "Creating task" << endl;
    cout << "Grid" << endl;
    rN_ = vm["N"].as<int>();
    N_ = rN_ + 6;
    xi_ = vm["xi"].as<double>();
    xf_ = vm["xf"].as<double>();
    dx_ = (xf_-xi_)/(N_-1.0);
    cout << N_ << " nodes from " << xi_ << " to " << xf_ << " with step " << dx_ << "." << endl;
    
    cout << "Alloc data memory" << endl;
    U_ = new Vector[N_];
    Unew_ = new Vector[N_];
    p = new double[N_];
    
    t_ = 0.0;
    CFL_ = vm["CFL"].as<double>();
    //end_t_ = vm["end_t"].as<double>();
    
    dt_ = 0.5*CFL_*dx_;
    cout << "CFL = " << CFL_ << endl;
    cout << "End time = " << end_t_ << endl;

    cout << "Task ready" << endl;
    
    WENO_K = 3;
    WENO_CRI = new double[WENO_K*WENO_K]{1./3.,5./6.,-1./6.,-1./6.,5./6.,1./3.,1./3.,-7./6.,11./6.};
    WENO_EPSILON = 1e-12;
    WENO_OMEGA = new double[WENO_K]{0.3,0.6,0.1};
}

gasEqSystem::~gasEqSystem()
{
    delete [] U_;
    delete [] Unew_;
    delete [] p;
    delete [] WENO_CRI;
    delete [] WENO_OMEGA;
}



void gasEqSystem::InitTask(const po::variables_map &vm, function<double(double)> nozzle)
{
    nozzleShape_ = nozzle;
    CONST_GAMMA  = vm["gamma"].as<double>();
    CONTROL_EPSILON  = vm["controlEps"].as<double>();
    end_t_ = vm["end_t"].as<double>();
    
    double rho0  = vm["rho0"].as<double>();
    double u0  = vm["u0"].as<double>();
    double p0  = vm["p0"].as<double>();
    
    double rhoi  = vm["rhoi"].as<double>();
    double ui  = vm["ui"].as<double>();
    double pi  = vm["pi"].as<double>();
    
    double rhof  = vm["rhof"].as<double>();
    double uf  = vm["uf"].as<double>();
    double pf  = vm["pf"].as<double>();
    
    Vector init(var::N);
    init[var::rho] = rho0;
    //init[var::m] = rho0*u0;
    //init[var::e] = p0/(rho0*(CONST_GAMMA-1.)) + u0*u0*0.5;
    for(int l = 0;l < rN_; l++)
    {
        U_[index(l)] = A(l)*init;
        p[index(l)] = p0;
    }
    init[var::rho] = rhoi;
    //init[var::m] = rhoi*ui;
    //init[var::e] = pi/(rhoi*(CONST_GAMMA-1.)) + ui*ui*0.5;
    U_[0] = A(0)*init;
    U_[1] = A(0)*init;
    U_[2] = A(0)*init;
    U_[3] = A(0)*init;
    p[0] = pi;
    p[1] = pi;
    p[2] = pi;
    p[3] = pi;
    init[var::rho] = rho0;//rhof;
    //init[var::m] = rhof*uf;
    //init[var::e] = pf/(rhof*(CONST_GAMMA-1.)) + uf*uf*0.5;
    U_[N_-1] = A(rN_-1)*init;
    U_[N_-2] = A(rN_-1)*init;
    U_[N_-2] = A(rN_-1)*init;
    U_[N_-3] = A(rN_-1)*init;
    p[N_-1] = pf;
    p[N_-2] = pf;
    p[N_-3] = pf;
    p[N_-4] = pf;
}


void gasEqSystem::Solve()
{
    cout << "Calculation start" << endl
         << "dt = " << dt_ << endl;
    
    file_ = ofstream("data.dat");
    step_ = 0;
    Save();
    bool run = true;
    while(run)
    {
        t_ += dt_;
        step_++;
        for(int l = 1;l < rN_-1; l++)
        {
            Vector Fpos = WENO_flux(index(l)); 
            Vector Fneg = WENO_flux(index(l)-1);
            cout <<  l << endl << Fpos << Fneg;
            cout << -dt_*(Fpos - Fneg)/dx_ + dt_*R(index(l));
            Unew_[index(l)] = U_[index(l)] - dt_*(Fpos - Fneg)/dx_ + dt_*R(index(l));
        }
        double Drho_Dt_max = 0;
        for(int l = 1;l < rN_-1; l++)
        {
            //Drho_Dt_max = max(Drho_Dt_max,fabs(Unew_[index(l)][var::rho] - U_[index(l)][var::rho])/Unew_[index(l)][var::rho]/dt_);
            U_[index(l)] = Unew_[index(l)];
            //double u = U_[index(l)][var::m]/U_[index(l)][var::rho]/A(l);
            //p[index(l)] = (CONST_GAMMA-1.)*(U_[index(l)][var::e]- 0.5*u*u)/A(l);
        }
        cout << step_ << " -- " << Drho_Dt_max << "-- " << step_ % 1000 << endl;
        //if (CONTROL_EPSILON > Drho_Dt_max) run = false;
        if (t_ > end_t_) run = false;
        //Save();
        if (step_ % 20 == 0) Save();
    }
    Save();
    file_.close();
    cout << "Calculation end" << endl;
}

void gasEqSystem::Save()
{
    std::cout << "Saving for t = " << t_ << "(" << step_ << ")" << std::endl;
    /*for(int l = 0;l < rN_; l++)
    {
        file_ << dx_*l << '\t' << A(l) << '\t'
                << U_[index(l)][var::rho]/A(l) << '\t' 
                << U_[index(l)][var::m]/U_[index(l)][var::rho]/A(l) << '\t'
                << p[index(l)] << endl;
    }*/
    for(int l = 0;l < rN_; l++)
    {
        file_ << dx_*l << '\t' << A(l) << '\t'
                << U_[index(l)][var::rho]
                << endl;
    }
    file_ << endl << endl;    
}
