set terminal qt 0
set grid
set xlabel "x"
set ylabel "{/Symbol r}"
do for [k=0:20] {
plot [-0.001:1.0001][0:2] "data.dat" u 1:3 i k w l lw 2 t "{/Symbol r}(x,t)" 
pause 0.3
}
