#include <iostream>
#include <fstream>
#include <fftw3.h>
#include <cmath>
#include <complex>

using namespace std;

typedef complex<double> Complex;

int main ()
{
    int n = 256;
    double omega = 1.0;
    
    double xi = 0.0;
    double xf = 14.5*M_PI;
    
    double* x = new double[n];
    double* signal = (double*)fftw_alloc_real(sizeof(double)*n);
    Complex* spectre = (Complex*)fftw_alloc_complex(sizeof(Complex)*n);
    double* result = new double[n];
    
    fftw_plan p = fftw_plan_dft_r2c_1d
                    (
                        n,
                        (double*)signal, 
                        (fftw_complex*)spectre,
                        FFTW_ESTIMATE
                    );
    //------------------------------------------------------------------
    double dx = (xf-xi)/n;
    ofstream fSq("squareF.dat");
    for(int i = 0; i < n; i++)
    {
        x[i] = dx*i;
        signal[i] = sin(omega*x[i]);
        fSq << x[i] << "\t\t" << signal[i] << endl;
    }
    fSq.close();
    //------------------------------------------------------------------
    fftw_execute(p);
  
    ofstream rSq("squareR.dat");
    for(int i = 0; i < n/2; i++)
    {
        result[i] = abs(spectre[i]);
        rSq << double(i)/(xf-xi) << "\t\t" << result[i] << endl;
    }
    rSq.close();
    //------------------------------------------------------------------
    ofstream fH("HF.dat");
    for(int i = 0; i < n; i++)
    {
        signal[i] = sin(omega*x[i])*0.5*(1.0 - cos(2.0*M_PI*i/n));
        fH << x[i] << "\t\t" << signal[i] << endl;
    }
    fH.close();
    //------------------------------------------------------------------
    fftw_execute(p);
    ofstream rH("HR.dat");
    for(int i = 0; i < n/2; i++)
    {
        result[i] = abs(spectre[i]);
        rH << double(i)/(xf-xi) << "\t\t" << result[i] << endl;
    }
    rH.close();
    //------------------------------------------------------------------
    fftw_destroy_plan(p);
    fftw_free(signal);
    fftw_free(spectre);
    delete [] x;
    delete [] result;
    
    
    return 0;
}

