/**********************************************************************\
 * 
 * class firstOrderODEsystem implementation
 * 
 * Description
 *      class for system of first order ordinary differential equation
 *      Ux = R(U)
 * 
 * HeaderFiles
 *      firstOrderODEsystem.H
 * 
\**********************************************************************/
#include "firstOrderODEsystem.H"

firstOrderODEsystem::firstOrderODEsystem( const VectorFunction R):
R_(R),
step_(0.0)
{}

firstOrderODEsystem::~firstOrderODEsystem()
{}



void firstOrderODEsystem::InitTask( const Vector& U0)
{
    U0_ = U0;
    step_ = 0;
    
}

void firstOrderODEsystem::InitSave(std::string name)
{
    name_ = name;
    file_ = std::ofstream(name_);
}

void firstOrderODEsystem::SolveEuler(double end, int nStep)
{
    U_ = U0_;
    
    double dx = (end - U_[0])/nStep;
    
    std::cout << "Solving Euler from " << U_[0] << " to " << end << " with dx = "
    << dx << std::endl;
    step_ = 0;
    Save();
    while(step_ < nStep)
    {
        Vector Uold = U_;
        Vector R = calcR(R_,U_);
        U_ = Uold + R*dx;
        step_++;
        Save();
    }
    file_.close();
}

void firstOrderODEsystem::SolveRungeKutta(double end, int nStep)
{
    U_ = U0_;
    
    double dx = (end - U_[0])/nStep;
    
    std::cout << "Solving RungeKutta from " << U_[0] << " to " << end << " with dx = "
    << dx << std::endl;
    step_ = 0;
    Save();
    while(step_ < nStep)
    {
        Vector Uold = U_;
        Vector R = calcR(R_,Uold + 0.5*dx*calcR(R_,Uold));
        U_ = Uold + R*dx;
        step_++;
        Save();
    }
    file_.close();
}

void firstOrderODEsystem::Save()
{
    std::cout << "Saving for x = " << U_[0] << "(" << step_ << ")" << std::endl;
    file_ << U_;
}
