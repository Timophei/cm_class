/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#include <boost/program_options.hpp>
#include "firstOrderODEsystem.H"

namespace po = boost::program_options;
using namespace std;

int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("N", po::value<int>(), "Number of nodes")
    ("xf", po::value<double>(), "End x coord");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
    
    double xf = vm["xf"].as<double>();
    int N = vm["N"].as<int>();
    
    enum
    {
        x,
        f,
        df,
        d2f
    };
    
    VectorFunction eqR(4);
    eqR[x] =    [&] (Vector U)
                {
                    return 1.0;
                };
    eqR[f] =    [&] (Vector U)
                {
                    return U[df];
                };
    eqR[df] =   [&] (Vector U)
                {
                    return U[d2f];
                }; 
    eqR[d2f] =  [&] (Vector U)
                {
                    return -0.5*U[f]*U[d2f];
                };
    firstOrderODEsystem eq1(eqR);
    Vector U0(4);
    U0[x]   = 0.0;
    U0[f]   = 0.0;
    U0[df]  = 0.0;
    U0[d2f] = 1.0;
    
    eq1.InitTask(U0);
    //eq1.InitSave(string("boundaryLayerE.dat"));
    //eq1.SolveEuler(xf,N);
    eq1.InitSave(string("boundaryLayerRK.dat"));
    eq1.SolveRungeKutta(xf,N);
    
    // NSU class task
    /*VectorFunction eqR2(2);
    eqR2[x] =   [&] (Vector U)
                {
                    return 1.0;
                };
    eqR2[f] =   [&] (Vector U)
                {
                    return -U[f];
                };
    firstOrderODEsystem eq2(eqR2);
    Vector U02(2);
    U02[x]   = 0.0;
    U02[f]   = 1.0;
    eq2.InitTask(U02);
    eq2.InitSave(string("task7E.dat"));
    eq2.SolveEuler(3.0,100);
    eq2.InitSave(string("task7RK.dat"));
    eq2.SolveRungeKutta(3.0,100);*/
    
    /*VectorFunction eqR3(3);
    eqR3[x] =   [&] (Vector U)
                {
                    return 1.0;
                };
    eqR3[1] =   [&] (Vector U)
                {
                    return 10.0*U[1] - 2.0*U[1]*U[2];
                };
    eqR3[2] =   [&] (Vector U)
                {
                    return 2*U[1]*U[2] - 10.0*U[2];
                };
    firstOrderODEsystem eq3(eqR3);
    Vector U03(3);
    U03[x]   = 0.0;
    U03[1]   = 10.0;
    U03[2]   = 10.0;
    eq3.InitTask(U03);
    eq3.InitSave(string("task8RK.dat"));
    eq3.SolveRungeKutta(1.0,100);*/
    
    
    return 0;
}

