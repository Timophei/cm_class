/**********************************************************************\
 * 
 * class firstOrderODEsystem declaration
 * 
 * Description
 *      class for system of first order ordinary differential equation
 *      Ux = R(U) where  U - extended solution vector
 * 
 *          / x  \
 *          | u1 |
 *      U = | u2 |
 *          |  . |
 *          |  . |
 *          \ un /
 * 
 *      R(U) - right-hand function vector
 * 
 *              /   1   \
 *              | f1(U) |
 *      R(U) =  | f2(U) |
 *              |   .   |
 *              |   .   |
 *              \ fn(U) / 
 * 
 * HeaderFiles
 *      firstOrderODEsystem.H
 * 
\**********************************************************************/
#pragma once
#include "VectorFunction.H"
#include <fstream>

class firstOrderODEsystem
{
    private:
    
        // Eq data 
        VectorFunction R_;
        Vector U0_;
        
        // Solution data
        Vector U_;
                
        int step_;
        int end_step_;
        
        std::string name_;
        
        std::ofstream file_;
            
    public:
    
        firstOrderODEsystem(const VectorFunction R);
        
        ~firstOrderODEsystem();
        
        void InitSave(std::string name);
        void InitTask(const Vector& U0);
        void Save();
        
        void SolveEuler(double end, int nStep);
        void SolveRungeKutta(double end, int nStep);
};
