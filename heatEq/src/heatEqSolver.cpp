#include "heatEqSolver.h"

heatEqSolver::heatEqSolver(const po::variables_map &vm)
{
    cout << "Setting task" << endl;
    cout << "Grid" << endl;
    N_ = vm["N"].as<int>();
    a_ = vm["a"].as<double>();
    b_ = vm["b"].as<double>();
    dx_ = (b_-a_)/(N_-1.0);
    dx_coeff_2 = (1.0/(dx_*dx_));
    cout << N_ << " nodes from " << a_ << " to " << b_ << " with step " << dx_ << "." << endl;
    
    cout << "Alloc data memory" << endl;
    U_ = new double[N_];
    memUsize_ = sizeof(double)*N_;
    cout << "data memory = " << memUsize_ << " byte."<< endl;
    
    t_ = 0.0;
    CFL_ = vm["CFL"].as<double>();
    end_t_ = vm["end_t"].as<double>();
    
    dt_ = 0.5*CFL_*dx_*dx_;
    cout << "CFL = " << CFL_ << endl;
    cout << "End time = " << end_t_ << endl;
    dataO.open("T.dat", std::fstream::out);
    
    cout << "Task ready" << endl;
}

heatEqSolver::~heatEqSolver()
{
    dataO.close();
    delete [] U_;
}

void heatEqSolver::Init()
{
    cout << "Init start data" << endl;
    for(int i = 0; i < N_; i++)
    {
        U_[i] = sin(M_PI*dx_*i);
    }
    cout << "Start data ready" << endl;
}

void heatEqSolver::Solve()
{
    cout << "Time loop" << endl;
    t_ = 0.0;
    while(t_ < end_t_)
    {
        //cout << "Time = " << t << endl;
        double* tU = new double[N_];
        memcpy(tU,U_,memUsize_);
        for(int i = 1; i < N_-1; i++)
        {
            U_[i] = tU[i] + (tU[i+1] - 2.0*tU[i] + tU[i-1])*dt_*dx_coeff_2;
        }
        delete [] tU;
        t_ += dt_;
    }
}

void heatEqSolver::Save()
{
    cout << "Saving for time = " << t_ << endl;
    for(int i = 0; i < N_ ; i++)
    {
        dataO << dx_*i << '\t' << U_[i] << endl;
    }
}
