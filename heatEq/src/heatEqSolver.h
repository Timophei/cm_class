#pragma once
#include <math.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

using namespace std;

class heatEqSolver
{
    private:
        // grid param
        int N_;
        double a_;
        double b_;
        double dx_;
        double dx_coeff_2; // 1/dx;
        
        // time param
        double t_;
        double dt_;
        double end_t_;
        
        // 
        double CFL_;
        
        // task data
        double* U_;
        int memUsize_;
        fstream dataO;
    
    public:
        
        heatEqSolver(const po::variables_map &vm);
        ~heatEqSolver();
        
        void Init();
        void Solve();
        void Save();
   
};
