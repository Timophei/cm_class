#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <cmath>
#include "Vector.H"
#include <functional>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

/* support func */
double q() { /* initial condition func for domain for t == 0*/
    return 0.0;
}
double q1(double y) { /*  */
    return y*y*y*y;
}
double q2(double y) { /*  */
    return 1.0-6.0*y*y + y*y*y*y;
}
double q3(double x) { /*  */
    return x*x*x*x;
}
double q4(double x) { /*  */
    return x*x*x*x-6.0*x*x+1.0;
}
double solution(double x, double y) { /* analitic silution for domain */
    return x*x*x*x - 6.0*x*x*y*y + y*y*y*y;
}

/* matrix eq solve */
Vector matrix_eq_solve(
    Vector& up,
    Vector& main,
    Vector& low,
    const Vector& U,
    Vector& R
){
    Vector b(U);
    int n = b.size();

    //n--; // since we start from x0 (not x1)
    up[0] /= main[0];
    b[0] /= main[0];

    for (int i = 1; i < n-1; i++) 
    {
        up[i] /= main[i] - low[i]*up[i-1];
        b[i] = (b[i] - low[i]*b[i-1]) / (main[i] - low[i]*up[i-1]);
    }

    b[n-1] = (b[n-1] - low[n-1]*b[n-2]) / (main[n-1] - low[n-1]*up[n-2]);

    for (int i = n-1; i-- > 0;) 
    {
        b[i] -= up[i]*b[i+1];
    }
    return b;
}

/* Main programm */

int main () {
    /* Read config */
    std::cout << "Read config file\n";
    std::ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("nx", po::value<int>(), "Number of nodes on x direction")
    ("ny", po::value<int>(), "Number of nodes on y direction")
    ("xi", po::value<double>(), "Start x coord")
    ("xf", po::value<double>(), "End x coord")
    ("yi", po::value<double>(), "Start y coord")
    ("yf", po::value<double>(), "End y coord")
    ("tau", po::value<double>(), "tau")
    ("eps", po::value<double>(), "eps");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();

    /* Space */
    int nx = vm["nx"].as<int>();
    int ny = vm["ny"].as<int>();
    double x_max = vm["xf"].as<double>();
    double x_min = vm["xi"].as<double>();
    double y_max = vm["yf"].as<double>();
    double y_min = vm["yi"].as<double>();
    double dx = (x_max - x_min)/static_cast<double>(nx-1);
    double dy = (y_max - y_min)/static_cast<double>(ny-1);
    /* Time */
    double tau = vm["tau"].as<double>();
    int iter = 1;
    /* Param */
    double eps = vm["eps"].as<double>();
    /* Data store*/
    int n = nx*ny;
    std::function<int(int,int)> pos( 
        [&] (int i, int j)
        {
            return i + ny*j;
        }
    );
    Vector T(n);
    /* const */
    double nu = M_1_PI;

    /* Task Info*/
    std::cout << "*** Task info ***\nGrid\n" 
                << "nx = " << nx << " X (" << x_min << ", " << x_max << ") dx = " << dx << "\n"
                << "ny = " << ny << " Y (" << y_min << ", " << y_max << ") dy = " << dy << "\n"
                << "Time\n";
    
    /* Init */
    for (int i = 0; i < nx; i++) {
        for (int j = 0; j < ny; j++) {
            T[pos(i,j)] = q();
        }
    }
    std::system(       /* lunux system call to create dir for result .csv files for Paraview */
        std::string("mkdir task4_" 
                    + std::to_string(nx) + "x" 
                    + std::to_string(ny) + "_t"
                    + std::to_string(tau) + "_e"
                    + std::to_string(eps)
                    ).c_str()
    );
    {   /* Save initial solution for t == 0 Note: using separated .csv files for each time step, save 3d grid with z = T */
        std::string filename("task4_" + std::to_string(nx) + "x" 
                        + std::to_string(ny) + "_t"
                        + std::to_string(tau) +  "_e"
                        + std::to_string(eps).c_str() + "/T.csv.0");
        std::ofstream file(filename);
        file << "x coord, y coord, z coord, T, dT\n";
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                file << dx*i << "," << dy*j << "," << T[pos(i,j)] << ","  << T[pos(i,j)] << ",0.0\n";
            }
        }
        file.close();
    }
    /* Create file to store resudual */
    std::string r_filename("task4_" + std::to_string(nx) + "x" 
                        + std::to_string(ny) + "_t"
                    + std::to_string(tau) + "_e"
                    + std::to_string(eps) + "/residual.dat");
    std::ofstream r_file(r_filename);

    /* Solve */
    bool run = true;
    while(run) { /* time loop */
        std::string filename("task4_" + std::to_string(nx) + "x" 
                    + std::to_string(ny) + "_t"
                    + std::to_string(tau) + "_e"
                    + std::to_string(eps) + "/T.csv."
                    + std::to_string(iter));
        std::ofstream file(filename);
        file << "x coord, y coord, z coord, T, dT\n";
        std::cout << "Time = " << iter << "\n";
        double cx = 0.5*tau/(dx*dx);
        double cy = 0.5*tau/(dy*dy);

        Vector T_0(T);

    /* Cacl BC*/
        for(int i = 0; i < nx; i++) {
            T[pos(i,0)] = q3(x_min + i*dx);
            T[pos(i,ny-1)] = q4(x_min + i*dx);
        }
        for(int j = 0; j < ny; j++) {
            T[pos(0,j)] = q1(y_min + j*dy);
            T[pos(nx-1,j)] = q2(y_min +j*dy);
        }

    /* Stage 1*/
        for (int j = 1; j < ny - 1; j++) { /* loop on row */
            Vector main(nx);
            Vector up(nx-1);
            Vector low(nx-1);
            Vector R(nx);
            Vector U(nx);

            U[0] = T[pos(0,j)]; 
            main[0] = 1.;
            up[0] = 0.;
            for (int i = 1; i < nx - 1; i++) {
                main[i] = 1 + 2*cx;
                low[i-1] = -cx;
                up[i] = -cx;
                R[i] = (1.0 - 2*cy)*T[pos(i,j)] + cy*(T[pos(i+1,j)] + T[pos(i-1,j)]);
                U[i] = T[pos(i,j)];
            }
            U[nx-1] = T[pos(nx-1,j)];
            main[nx-1] = 1.;
            low[nx-2] = 0.;
            Vector T_ = matrix_eq_solve(up, main, low, U, R);
            for (int i = 0; i < nx; i++) {
                T[pos(i,j)] = T_[i];
            }

        } /* end loop on row */

    /* Stage 2*/
        for (int i = 1; i < nx - 1; i++) { /* loop on col*/
            Vector main(ny);
            Vector up(ny-1);
            Vector low(ny-1);
            Vector R(ny);
            Vector U(ny);
            U[0] = T[pos(i,0)]; 
            main[0] = 1.;
            up[0] = 0.;
            for (int j = 1; j < ny - 1; j++) {
                main[j] = 1 + 2*cy;
                low[j-1] = -cy;
                up[j] = -cy;
                R[j] = (1.0 - 2*cx)*T[pos(i,j)] + cx*(T[pos(i,j+1)] + T[pos(i,j-1)]);
                U[j] = T[pos(i,j)];
            }
            U[ny-1] = T[pos(i,ny-1)];
            main[ny-1] = 1.;
            low[ny-2] = 0.;
            Vector T_ = matrix_eq_solve(up, main, low, U, R);
            for (int j = 0; j < ny; j++) {
                T[pos(i,j)] = T_[j];
            }
        } /* end loop on col */

    /* Save Time Step and cacl resudual */
        double residual = 0.0;
        /* Save solution for t Note: using separated .csv files for each time step, save 3d grid with z = 0.0 */
        for (int i = 0; i < nx; i++) {
            for (int j = 0; j < ny; j++) {
                double r_local = fabs(solution(x_min + dx*i,y_min + dy*j) - T[pos(i,j)]);
                residual = std::max(r_local,residual);
                file << x_min + dx*i << "," << y_min + dy*j << "," << T[pos(i,j)] << ","  
                    << T[pos(i,j)]  << ","<< r_local <<"\n";
            }
        }
        r_file << iter << "\t" << residual << "\n";
        file.close();
        iter++;
        double dT_max = 0;
        double eps_T_max = 0;
        for (int i = 0; i < n; i++) {
            dT_max = std::max(dT_max, fabs(T[i] - T_0[i]) );
            eps_T_max = std::max(eps_T_max, eps*fabs(T_0[i]) );
        }
        run = (dT_max > eps_T_max);
        std::cout << "residual = " << residual << " dT_max " << dT_max << "\n";
    } /* enf time loop */
    r_file.close();
    return 0;
}
