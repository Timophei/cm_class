set grid
set title "{/symbol d}(t)"
set xlabel "t"
plot [-0.001:1.001] "./task3_5x5_t10/residual.dat" using 1:2 with linespoints ls 5 title '5x5 10' ,\
"./task3_10x10_t40/residual.dat" using 1:2 with linespoints ls 4 title '10x10 40' ,\
"./task3_20x20_t160/residual.dat" using 1:2 with linespoints ls 3 title '20x20 160'
