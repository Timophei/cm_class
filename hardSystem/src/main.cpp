#include <iostream>
#include <fstream>

using namespace std;

class matrix
{
    public:
    double a_;
    double b_;
    double c_;
    double d_;
    
    matrix(double a, double b, double c, double d)
    {
        a_ = a; b_ = b; c_ = c; d_ = d;
    }
    double det()
    {
        return a_*d_ - b_*c_;
    }
    matrix Inv()
    {
        double det_ = det();
        return matrix(d_/det_,-b_/det_,-c_/det_,a_/det_);
    }
};

class vector
{
    public:
    double u_;
    double v_;
    
    vector(double u, double v)
    {
        u_ = u; v_ = v;
    }
};

vector operator* (const matrix & m, const vector v)
{
    double x = m.a_*v.u_ + m.b_*v.v_;
    double y = m.c_*v.u_ + m.d_*v.v_;
    return vector(x,y);
}


int main ()
{
    double dt = .001;
    double t = 0.0;
    
    double a = 998.;
    double b = 1998.;
    double c = 999.;
    double d = 1999.;
    
    ofstream f("phase.dat");
    
    while (t < 200)
    {
         
        vector u(0.1,0.1);
        matrix A(1.0-a*t, -b*t, c*t, 1.0+d*t);

        
        matrix B = A.Inv();
        
        u = B*u;
        
        f << u.u_ << '\t' << u.v_  << endl;
        t += dt;
    }
    f.close();
    
    return 0;
}

