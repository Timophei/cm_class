namespace po = boost::program_options;

using namespace std;

int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("N", po::value<int>(), "Number of nodes")
    ("a", po::value<double>(), "Start x coord")
    ("b", po::value<double>(), "End x coord")
    ("end_t", po::value<double>(), "End time")
    ("CFL", po::value<double>(), "CFL number");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
    
    return 0;
}

