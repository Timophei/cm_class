#include "InterpolationPolynom.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

using namespace std;


int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("N", po::value<int>(), "Number of nodes");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
    
    int n = vm["N"].as<int>();
    cout << n << endl;
    vector<double> x(n+1);
    vector<double> y(n+1);
    
    for(int k = 0; k < n+1; k++)
    {
        x[k] = M_PI_4*k/double(n);
        y[k] = sin(x[k]);
        cout << k << "-" << x[k] << "\t" << y[k] << endl;
    }
    
    
    InterpolationPolynom p(x, y);
    
    
    ofstream err("error.dat");
    ofstream graph("graph.dat");
    double dt = (x[n-1] - x[0])/double(10*n);
    //cout << dt << endl;
    for(int i = 0; i < 10*n; i++)
    {
        double t = i*dt;
        err << t << "\t" << p.value(t) - sin(t) << endl;
        graph << t << "\t" << p.value(t) << "\t" << sin(t) << endl;
    }
    
    err.close();
    graph.close();
    
    return 0;
}

