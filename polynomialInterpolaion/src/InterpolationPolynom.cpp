#include "InterpolationPolynom.h"

InterpolationPolynom::InterpolationPolynom(const vector<double>& x, const vector <double>& y):
grid(x)
{
    int n = x.size();
    
    //if (n != y.size()) throw 
    
    coeff_ = vector<double>(n);
    
    for(int i = 0; i < n; i++)
    {
        double c = 1.0;
        for (int j = 0; j < n; j++)
        {
            if (i != j) c *= (x[i] - x[j]);
        } 
        coeff_[i] = y[i]/c;
    }
    
}

InterpolationPolynom::~InterpolationPolynom()
{   
}

double InterpolationPolynom::value(double x)
{
    int n = grid.size();
    double result = 0.0;
    for(int i = 0; i < n; i++)
    {
        double r = 1.0;
        for (int j = 0; j < n; j++)
        {
            if (i != j) r *= (x - grid[j]);
        } 
        result += r*coeff_[i];
    }
    return result;
}
