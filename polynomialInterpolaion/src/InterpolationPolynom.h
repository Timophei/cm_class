#include <vector>

using namespace std;

class InterpolationPolynom
{
    private:
        const vector<double> & grid;
        vector<double> coeff_;
        
    public:
    
        InterpolationPolynom(const vector<double>& x, const vector<double>& y);
        InterpolationPolynom(const InterpolationPolynom & p);
        ~InterpolationPolynom();
        
        // Member function
        double value(double x);
};
