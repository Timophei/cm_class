set grid
set xlabel "x"
i = 0
f = 50
do for [k=i:f] {
print k
set terminal qt 0
plot [-0.001:1.001][0:2] "data.dat" u 1:2 i k w l lw 2 t "{/symbol r}(x,t)"
set terminal qt 1
plot [-0.001:1.001][0:6] "data.dat" u 1:3 i k w l lw 2 t "{u}(x,t)"
set terminal qt 2
plot [-0.001:1.001][0:13] "data.dat" u 1:4 i k w l lw 2 t "{p}(x,t)"
pause 0.1
}
