/**********************************************************************\
 * 
 * type Vector  
 * 
 * Description
 *      std::vector template specialization for double
 *      with mathematical operations and functions
 * 
\**********************************************************************/

#include "Vector.H"

/**********************************************************************\
 * 
 * Operations
 * 
\**********************************************************************/ 
Vector operator+(const Vector& v1, const Vector& v2)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]+v2[i];
    }
    return std::move(res);
}

Vector operator+(const Vector& v1, Vector&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] += v1[i];
    }
    return std::move(v2);
}

Vector operator+(Vector&& v1, const Vector& v2)
{
    return std::move(v2+v1);
}

Vector operator+(Vector&& v1, Vector&& v2)
{
    return std::move(v1+v2);
}

Vector operator-(const Vector& v1, const Vector& v2)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]-v2[i];
    }
    return std::move(res);
}

Vector operator-(const Vector& v1, Vector&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] = v1[i]-v2[i];
    }
    return std::move(v2);
}

Vector operator-(Vector&& v1, const Vector& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] = v1[i]-v2[i];
    }
    return std::move(v1);
}

Vector operator-(Vector&& v1, Vector&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] = v1[i]-v2[i];
    }
    return std::move(v1);
}

Vector operator+(const Vector& v1, double a)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]+a;
    }
    return std::move(res);
}

Vector operator+(Vector&& v1, double a)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] += a;
    }
    return std::move(v1);
}

Vector operator+(double a, const Vector& v1)
{
    return std::move(v1+a);
}

Vector operator+(double a, Vector&& v1)
{
    return std::move(v1+a);
}

Vector operator/(const Vector& v1, double a)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]/a;
    }
    return std::move(res);
}

Vector operator/(double a, const Vector& v1)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = a/v1[i];
    }
    return std::move(res);
}

Vector operator/(const Vector& v1, const Vector& v2)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]/v2[i];
    }
    return std::move(res);
}

Vector operator*(const Vector& v1, double a)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*a;
    }
    return std::move(res);
}

Vector operator*(Vector&& v1, double a)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v1[i] *= a;
    }
    return std::move(v1);
}

Vector operator*(double a, const Vector& v1)
{
    return std::move(v1*a);
}

Vector operator*(double a, Vector&& v1)
{
    return std::move(v1*a);
}

Vector operator*(const Vector& v1, const Vector& v2)
{
    Vector res(v1.size());
    for(int i = 0 ; i < v1.size() ; i++)
    {
        res[i] = v1[i]*v2[i];
    }
    return std::move(res);
}

Vector operator*(const Vector& v1, Vector&& v2)
{
    for(int i = 0 ; i < v1.size() ; i++)
    {
        v2[i] *= v1[i];
    }
    return std::move(v2);
}

Vector operator*(Vector&& v1, const Vector& v2)
{
    return std::move(v2*v1);
}


/**********************************************************************\
 * 
 * Functions
 * 
\**********************************************************************/
Vector sq(const Vector& v)
{
    Vector v2(v.size());
    for(int i = 0 ; i < v.size() ; i++)
    {
        v2[i] = v[i]*v[i];
    }
    return v2;
}
double norm(const Vector& v)
{
    double norm_val=0;
    for(auto val:v)
    {
        norm_val+=val*val;
    }
    return std::sqrt(norm_val);
}

double integral(const Vector& v, double dx)
{
    double norm_val=0.;
    for(auto val:v)
    {
        norm_val+=dx*val*val;
    }
    return norm_val;
}
double cmpSum(const Vector& v)
{
    double sum=0.;
    for(auto val:v)
    {
        sum+=val;
    }
    return sum;
}

void copyArray(const Vector* t, Vector* d, int n)
{
    for (int i = 0; i < n; i++)
    {
        d[i] = Vector(t[i]);
    }
}
/**********************************************************************\
 * 
 * Output
 * 
\**********************************************************************/
std::ostream& operator<<(std::ostream& out, const Vector &vec)
{
    for(auto v: vec)
    {
        out << std::setprecision(15) << v << "\t\t";
    }
    out << std::endl;
    return out;
}
