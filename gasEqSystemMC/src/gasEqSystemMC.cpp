/**********************************************************************\
 * 
 * class gasEqSystem implementation
 * 
 * Description
 *      class for system of first order ordinary differential equation
 *      Ut + Fx = R
 * 
 * HeaderFiles
 *      gasEqSystem.H
 * 
\**********************************************************************/
#include "gasEqSystemMC.H"

using namespace std;

/**********************************************************************\
 * low level fuction
\**********************************************************************/
void gasEqSystem::Flux(const Vector* v, Vector* f)
{
    for (int i = 0; i < N_; i++)
    {
        Vector tf(var::N);
        Vector tv = v[i];
        
        double a = A(i);
        double rho = tv[var::rho]/a;
        double u = tv[var::m]/rho/a;
        double rhoE = tv[var::e]/a;
        double p = (CONST_GAMMA-1.)*(rhoE - 0.5*u*u*rho);
        
        tf[var::rho] = rho*u;
        tf[var::m] = rho*u*u + p;
        tf[var::e] = (rhoE + p)*u;
        
        f[i] = a*tf;
        //cout << i << "  " << f[i];
    }
}

void gasEqSystem::Rhand(const Vector* v, Vector* r)
{
    for (int i = 0; i < N_; i++)
    {
        Vector tr(var::N);
        Vector tv = v[i];
        
        double a = A(i);
        double rho = tv[var::rho]/a;
        double u = tv[var::m]/rho/a;
        double rhoE = tv[var::e]/a;
        double p = (CONST_GAMMA-1.)*(rhoE - 0.5*u*u*rho);
        
        tr[var::rho] = 0.;
        tr[var::m] = -2.0*p*(1.-2.0*dx_*i);
        tr[var::e] = 0.;
        
        r[i] = tr;
        //cout << i << "  " << f[i];
    }
}

double gasEqSystem::A(int i)
{
    return nozzleShape_(dx_*i);
}


/**********************************************************************\
 * constructor
\**********************************************************************/
gasEqSystem::gasEqSystem(const po::variables_map &vm)
{
    cout << "Creating task" << endl;
    cout << "Grid" << endl;
    N_ = vm["N"].as<int>();
    xi_ = vm["xi"].as<double>();
    xf_ = vm["xf"].as<double>();
    dx_ = (xf_-xi_)/(N_);
    cout << N_ << " nodes from " << xi_ << " to " << xf_ << " with step " << dx_ << "." << endl;
    
    cout << "Alloc data memory" << endl;
    U_ = new Vector[N_];
    
    t_ = 0.0;
    CFL_ = vm["CFL"].as<double>();
    
    dt_ = 0.5*CFL_*dx_;
    cout << "CFL = " << CFL_ << endl;
    //cout << "End time = " << end_t_ << endl;

    cout << "Task ready" << endl;
}

gasEqSystem::~gasEqSystem()
{
    delete [] U_;
}



void gasEqSystem::InitTask(const po::variables_map &vm, function<double(double)> nozzle)
{
    nozzleShape_ = nozzle;
    CONST_GAMMA  = vm["gamma"].as<double>();
    CONTROL_EPSILON  = vm["controlEps"].as<double>();
    end_t_ = vm["end_t"].as<double>();
    Chi = vm["Chi"].as<double>();
    
    double rho0  = vm["rho0"].as<double>();
    double u0  = vm["u0"].as<double>();
    double p0  = vm["p0"].as<double>();
    
    double rhoi  = vm["rhoi"].as<double>();
    double ui  = vm["ui"].as<double>();
    double pi  = vm["pi"].as<double>();
    
    double rhof  = vm["rhof"].as<double>();
    double uf  = vm["uf"].as<double>();
    double pf  = vm["pf"].as<double>();
    
    Vector init(var::N);
    init[var::rho] = rho0;
    init[var::m] = rho0*u0;
    init[var::e] = p0/(CONST_GAMMA-1.) + rho0*u0*u0*0.5;
    for(int l = 0;l < N_; l++)
    {
        U_[l] = A(l)*init;
    }
    init[var::rho] = rhoi;
    init[var::m] = rhoi*ui;
    init[var::e] = pi/(CONST_GAMMA-1.) + rhoi*ui*ui*0.5;
    U_[0] = A(0)*init;
    for(int l = 0;l < 500; l++)
    {
        U_[l] = A(l)*init;
    }
    init[var::rho] = rhof;
    init[var::m] = rhof*uf;
    init[var::e] = pf/(CONST_GAMMA-1.) + rhof*uf*uf*0.5;
    for(int l = 500;l < N_; l++)
    {
        U_[l] = A(l)*init;
    }
    U_[N_-1] = A(N_-1)*init;
}

double gasEqSystem::CalcDt()
{
    double maxU = 0;
    for(int l = 0;l < N_; l++)
    {
        double a = A(l);
        double rho = U_[l][var::rho]/a;
        double u = U_[l][var::m]/rho/a;
        double rhoE = U_[l][var::e]/a;
        double p = (CONST_GAMMA-1.)*(rhoE - 0.5*u*u*rho);
        double c = sqrt(CONST_GAMMA*p/rho);
        maxU = max(maxU,fabs(u) + c);
    }
    return CFL_*dx_/maxU;
}

void gasEqSystem::Solve()
{
    cout << "Calculation start" << endl;
    
    file_ = ofstream("data.dat");
    step_ = 0;
    savingCount_ = 0;
    Save();
    bool run = true;

    Vector* F = new Vector[N_];
    Vector* R = new Vector[N_];
    Vector* Un = new Vector[N_];
    double* rhoOld = new double[N_];
    dt_ = CalcDt();
    
    while(run)
    {
        dt_ = CalcDt();
        t_ += dt_;
        step_++;
        //cout << std::setprecision(15) << "Time = " << t_ <<" dt = " << dt_ << endl;
        // MC 1 stage
        Flux(U_,F);
        Rhand(U_,R);
        for(int l = 1;l < N_-1; l++)
        {
            rhoOld[l] = U_[l][var::rho]/A(l);
            Un[l] = U_[l] - dt_*(F[l+1] - F[l])/dx_ + R[l]*dt_;
            //if (l == 1 ) cout << Un[l] << dt_*(F[l+1] - F[l])/-dx_ << R[l]*dt_ << endl;
        }
        Un[0] = U_[0];
        Un[N_-1] = U_[N_-1];
        // MC 2 stage
        Flux(Un,F);
        Rhand(Un,R);
        for(int l = 1;l < N_-1; l++)
        {
            U_[l] = 0.5*(U_[l] + Un[l] - dt_*(F[l] - F[l-1])/dx_ + R[l]*dt_);
            //cout << l << "  " << U_[l];
        }
        // Avis crap
        double Drho_Dt_max = 0;
        for(int l = 1;l < N_-1; l++)
        {
            double a = A(l);
            double v0 = U_[l][var::m]/U_[l][var::rho]/a;
            double vp = U_[l+1][var::m]/U_[l+1][var::rho]/a;
            double vn = U_[l-1][var::m]/U_[l-1][var::rho]/a;
            Vector pos = fabs(vp-v0)*(U_[l+1]-U_[l]);
            Vector neg = fabs(v0-vn)*(U_[l]-U_[l-1]);
            U_[l] = U_[l] + Chi*dt_*(pos-neg)/dx_;
            Drho_Dt_max = max(Drho_Dt_max,fabs(U_[l][var::rho]/a -rhoOld[l])/U_[l][var::rho]/a/dt_);
        }
        //U_[0] = U_[1];
        //U_[N_-1] = U_[N_-2];
        if (CONTROL_EPSILON > Drho_Dt_max) run = false;
        if (t_ > end_t_) run = false;
        if (step_ % 1000 == 0) 
        {
            Save();
            cout << "Drho_Dt_max = "<< Drho_Dt_max << endl;
        }
        
    }
    Save();
    file_.close();
    delete [] Un;
    delete [] F;
    delete [] rhoOld;
    delete [] R;
    cout << "Calculation end" << endl;
}

void gasEqSystem::Save()
{
    savingCount_++;
    std::cout << "Saving for t = " << t_ << "(" << savingCount_ << ")" << std::endl;
    for(int l = 0;l < N_; l++)
    {
        double a = A(l);
        double rho = U_[l][var::rho]/a;
        double u = U_[l][var::m]/rho/a;
        double rhoE = U_[l][var::e]/a;
        double p = (CONST_GAMMA-1.)*(rhoE - 0.5*u*u*rho);
        
        file_ << dx_*l << '\t' << rho << '\t' << u << '\t' << p << endl;
    }
    file_ << endl << endl;    
}
