/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
#include <boost/program_options.hpp>
#include "gasEqSystemMC.H"

namespace po = boost::program_options;
using namespace std;

int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("N", po::value<int>(), "Number of nodes")
    ("xi", po::value<double>(), "Start x coord")
    ("xf", po::value<double>(), "End x coord")
    ("rhoi", po::value<double>(), "rho in xi")
    ("ui", po::value<double>(), "u in xi")
    ("pi", po::value<double>(), "p in xi")
    ("rhof", po::value<double>(), "rho in xf")
    ("uf", po::value<double>(), "u in xf")
    ("pf", po::value<double>(), "p in xf")
    ("rho0", po::value<double>(), "rho internal")
    ("u0", po::value<double>(), "u internal")
    ("p0", po::value<double>(), "p internal")
    ("gamma", po::value<double>(), "gamma")
    ("controlEps", po::value<double>(), "controlEps")
    ("CFL", po::value<double>(), "CFL")
    ("end_t", po::value<double>(), "CFL")
    ("Chi", po::value<double>(), "Chi");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
    
    double xf = vm["xf"].as<double>();
    int N = vm["N"].as<int>();
    
    gasEqSystem task(vm);
    task.InitTask
    (
        vm,
        [&] (double x)
        {
            return 0.5 + (0.5)*(1 - 2.*x)*(1 - 2.*x);
        }
    );
    /*task.InitTask
    (
        vm,
        [&] (double x)
        {
            return 1;
        }
    );*/
    task.Solve();
    
    return 0;
}

