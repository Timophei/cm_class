#include "SEnullFunc.h"
#include <iomanip> 

namespace po = boost::program_options;

using namespace std;


int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("U0", po::value<double>(), "U0")
    ("a", po::value<double>(), "a")
    ("residual", po::value<double>(), "residual")
    ("d_min", po::value<double>(), "d_min")
    ("d_max", po::value<double>(), "d_max")
    ("si_x0", po::value<double>(), "x0");
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
       
    SEnullFunc task(vm);
    
    cout << task.dichotomy(vm["d_min"].as<double>(),vm["d_max"].as<double>()) << endl;
    
    cout << "x0 loop " << endl;
    ofstream n("null.txt");
    double x0 = -100;
    while (x0 < 0)
    {
        cout << setprecision(8)<< " x0 = " << x0 << "\t N x = " << task.Newton(x0) << "\t\t SI x= " << task.simpleIter(x0) << endl;
        x0 -= vm["U0"].as<double>()/50.;
        n << task.Newton(x0) << "\t" << task.f(task.Newton(x0)) << "\t" << task.f(task.simpleIter(x0)) << endl;
    }
    n.close();
    
    ofstream f("f.txt");
    double x = -100.0;
    double dx = 0.1;
    while (x < 0.0)
    {
        f << setprecision(8) << x << "\t" << task.f(x) << "\t" << task.f(task.Newton(x)) << endl;
        x +=dx;
    }
    f.close();
    return 0;
}

