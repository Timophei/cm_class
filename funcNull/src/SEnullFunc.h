#pragma once
#include <math.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

using namespace std;

class SEnullFunc
{
    private:
        double U0_;
        double a_;
        double residualLinit_;
    
    public:
        SEnullFunc(const po::variables_map &vm);
        ~SEnullFunc();
        
        double dichotomy(const double min, const double max);
        double simpleIter(const double x0);
        double Newton(const double x0);
        double f(const double x);
};
