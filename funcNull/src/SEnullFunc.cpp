#include "SEnullFunc.h"

SEnullFunc::SEnullFunc(const po::variables_map &vm)
{
    U0_ = vm["U0"].as<double>();
    a_ = vm["a"].as<double>();
    residualLinit_ = vm["residual"].as<double>();
}

SEnullFunc::~SEnullFunc()
{
    // Do nothing
}

double SEnullFunc::dichotomy(const double min, const double max)
{
    double a = min;
    double b = max;
    double x = 0.0;
    double res = 1.0;
    int n = log2(((b-a)/residualLinit_)) + 100;
    cout << "Dichotomy iteration for residual " << residualLinit_  << " is " << n << endl;
    for (int i = 0; i < n;i++)
    {
        double c = 0.5*(b + a);
        //cout << f(a) << " " << f(c) << "  " << f(b) << endl;
        if (f(a)*f(c) > 0.0) a = c;
        if (f(c)*f(b) > 0.0) b = c;
        res = fabs(x-c);
        x = c;
    }
    return x;
}

double SEnullFunc::simpleIter(const double x0)
{
    //cout << "Simple iteration" << endl;
    double x = x0;
    double res = 1.0;
    do
    {
        double c = x + f(x);
        res = fabs(x-c);
        x = c;
        //cout << c << " " << x << " " << res << endl;
        if (fabs(c) < residualLinit_) break;
    }
    while(res > residualLinit_);
    return x;
    
}

double SEnullFunc::Newton(const double x0)
{
    //cout << "Newton iteration" << endl;
    double x = x0;
    double k = (f(x+residualLinit_) - f(x))/(residualLinit_);
    double res = 1.0;
    do
    {
        double c = x - f(x)/k;
        k = (f(x+residualLinit_) - f(x))/(residualLinit_);
        res = fabs(x-c);
        x = c;
        //cout << c << " " << x << " " << res << endl;
        if (fabs(c) < residualLinit_) break;
    }
    while(res > residualLinit_);
    return x;
    
}
//----------------------------------------------------------------------
double SEnullFunc::f(const double x)
{
    double dE = fabs(U0_ - fabs(x));
    double f = tan(a_*sqrt(2*dE)) - sqrt(-U0_/dE);
    return f;
}
