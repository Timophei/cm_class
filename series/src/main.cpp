#include <iostream>
#include <bitset>
#include <math.h> 
#include <iomanip> 

using namespace std;

int main ()
{
const int N = 10000;
const double val = -0.6930971830599453;
//cout << "from N to 0" << endl;
double sum = 0.0;
double sign = -1.0;
for(int n = 1; n < N+1; n++)
{
    //sign *= sign;
    sum += pow(-1.0,double(n))/double(n);
    //cout << "n = " << n << " sign = " << pow(-1.0,double(n)) << " sum = " << sum << endl;
}
cout << setprecision(16)<< " sum = " << sum << " " << bitset<64>(*reinterpret_cast<unsigned long int*>(&sum)) << " err= " << sum - val<<  endl;
//cout << "from 0 to N" << endl;
sum = 0.0;
sign = -1.0;
for(int n = N; n > 0; n--)
{
    //sign *= sign;
    sum += pow(-1.0,double(n))/double(n);
    //cout << "n = " << n << " sign = " << pow(-1.0,double(n)) << " sum = " << sum << endl;
}
cout << setprecision(16)<< " sum = " << sum << " " << bitset<64>(*reinterpret_cast<unsigned long int*>(&sum)) << " err= " << sum - val<<  endl;
sum = 0.0;
double sump = 0.0;
double sumn = 0.0;
for(int n = 1; n < N+1; n = n + 2)
{
    sumn += -1.0/double(n);
} 
for(int n = 2; n < N+1; n = n + 2)
{
    sumn += 1.0/double(n);
}     
sum = sump + sumn;
cout << setprecision(16)<< " sum = " << sum << " " << bitset<64>(*reinterpret_cast<unsigned long int*>(&sum)) << " err= " << sum - val<<  endl;

sum = 0.0;
 sump = 0.0;
 sumn = 0.0;
for(int n = N; n > 0; n = n - 2)
{
    sump += 1.0/double(n);
}
for(int n = N - 1; n > 0; n = n - 2)
{
    sumn += -1.0/double(n);
}    
sum = sump + sumn;
cout << setprecision(16)<< " sum = " << sum << " " << bitset<64>(*reinterpret_cast<unsigned long int*>(&sum)) << " err= " << sum - val<<  endl;

    return 0;
}

