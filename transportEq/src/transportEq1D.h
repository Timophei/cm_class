/**********************************************************************\
 * 
 * class transportEq1D declaration
 * 
 * Description
 *      Differential equation Ut + a Ux = 0
 *      Implicit and Explicit solution method avaliable
 * 
 * SourceFiles
 *      transportEq1D.cpp
 * 
\**********************************************************************/
#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <functional>
#include <boost/program_options.hpp>
#include "diagonal3Matrix.h"
#include "matrixEq.h"

namespace po = boost::program_options;

using namespace std;


class transportEq1D
{
    private:
        // grid param
        int N_;
        double xi_;
        double xf_;
        double dx_;
        double dx_coeff_; // 1/dx;
        
        // time param
        double t_;
        double dt_;
        double save_dt_;
        double end_t_;
        int step_;
        
        // 
        double CFL_;
        
        // task data
        double a_;
        Vector U_;
        string name_;
    
        bool timeloopNoSave();
        bool timeloopSave();
    
    public:
    
        transportEq1D(const po::variables_map &vm);
        ~transportEq1D();
        
        void Init();
        void InitialCondition(const char* name, const function<double(double)> & f);
        void Save();
        
        void ImplicitSolve(); // matrix Eq
        void ExplicitSolve(); // time inegration
    
};
