/**********************************************************************\
 * 
 * class transportEq1D implementation
 * 
 * Description
 *      Differential equation Ut + a Ux = 0
 *      Implicit and Explicit solution method avaliable
 *      
 * 
 * HeaderFiles
 *      transportEq1D.h
 * 
\**********************************************************************/
#include "transportEq1D.h"

transportEq1D::transportEq1D(const po::variables_map &vm):
U_(vm["N"].as<int>())
{
    cout << "Setting task" << endl;
    cout << "Grid" << endl;
    N_ = vm["N"].as<int>();
    xi_ = vm["xi"].as<double>();
    xf_ = vm["xf"].as<double>();
    dx_ = (xf_-xi_)/(N_-1.0);
    dx_coeff_ = 1.0/dx_;
    //dx_coeff_2 = (1.0/(dx_*dx_));
    cout << N_ << " nodes from " << xi_ << " to " << xf_ << " with step " << dx_ << "." << endl;
    
    a_ = vm["a"].as<double>();
    
    t_ = 0.0;
    CFL_ = vm["CFL"].as<double>();
    end_t_ = vm["end_t"].as<double>();
    step_ = 0;
    
    dt_ = CFL_*dx_/a_;
    cout << "CFL = " << CFL_ << endl;
    cout << "End time = " << end_t_ << endl;
    
    cout << "Task ready" << endl;
}

transportEq1D::~transportEq1D()
{
    
}

void transportEq1D::InitialCondition(const char* name, const function<double(double)> & f)
{
    cout << "Init start data" << endl;
    t_ = 0.0;
    step_ = 0;
    dt_ = CFL_*dx_/a_;
    for(int i = 0; i < N_; i++)
    {
        double x = dx_*i;
        U_[i] = f(x);
    }
    name_ = string(name);
    cout << "Start data ready" << endl;
}

void transportEq1D::ImplicitSolve()
{
    cout << "ImplicitSolve" << endl;
    
    Vector main(N_); // initial value = 0.0 for all elements.
    Vector low(N_-1);
    Vector up(N_-1);
    Vector b(N_);
    
    cout << "Time loop" << endl;
    while(t_ < end_t_)
    {
        dt_ = min(dt_, end_t_ - t_);
        t_ += dt_;
        step_++;
        
        cout << "Time = " << t_ << "(" << step_ << ")" << "\t dt = " << dt_ << endl;
        
        double C = a_*dt_*dx_coeff_;
        
        cout << -C/(1.0 + C) << "   " << 1/(1.0 + C)<< endl;
        
        for (int i = 1; i < N_; i++)
        {
            main[i] = 1.0 + C;
        }
        main[0] = 1.0; // fixed value BC of xi
        for (int i = 0; i < N_-1; i++)
        {
            low[i] = -C;
        }
        
        //diagonal3Matrix A(main,low,up);
        
        //A = A/(1.0 + C); 
        b = U_;

        //matrixEq<diagonal3Matrix> Eq(A,U_,b);

        //U_ = Eq.Solve();
        
            //n--; // since we start from x0 (not x1)
            up[0] /= main[0];
            b[0] /= main[0];

            for (int i = 1; i < N_-1; i++) 
            {
                up[i] /= main[i] - low[i]*up[i-1];
                b[i] = (b[i] - low[i]*b[i-1]) / (main[i] - low[i]*up[i-1]);
            }

            b[N_-1] = (b[N_-1] - low[N_-1]*b[N_-2]) / (main[N_-1] - low[N_-1]*up[N_-2]);

            for (int i = N_-1; i-- > 0;) 
            {
                b[i] -= up[i]*b[i+1];
            }
            U_ = b;
    }
}

void transportEq1D::ExplicitSolve()
{
    cout << "ExplicitSolve" << endl;
    cout << "Time loop" << endl;
    while(t_ < end_t_)
    {
        dt_ = min(dt_, end_t_ - t_);
        t_ += dt_;
        step_++;
        
        cout << "Time = " << t_ << "(" << step_ << ")" << "\t dt = " << dt_ << endl;
        
        Vector tU = U_;
        for(int i = 1; i < N_; i++)
        {
            U_[i] = tU[i] - a_*(tU[i] - tU[i-1])*dt_*dx_coeff_;
        }
    }
}

void transportEq1D::Save()
{
    cout << "Saving for time = " << t_ << "(" << step_ << ")" << endl;
    string timeName(name_ + to_string(t_) + ".dat");
    ofstream f(timeName);
    for(int i = 0; i < N_ ; i++)
    {
        f << dx_*i << '\t' << U_[i] << endl;
    }
    f.close();
}
