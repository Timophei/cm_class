/**********************************************************************\
 * 
 * template class matrixEq
 * 
 * Description
 *      matrix equation type Ax = b
 *      A - matrix
 *      b, x - Vectors
 *      for all marix classes with defined multiply on Vector operator*
 *      using BiCGStab method
 * 
 * HeaderFiles
 *      matrixEq.h
 * 
\**********************************************************************/

template <typename Type>
matrixEq<Type>::matrixEq(Type & A,const Vector& x, const Vector& b):
A_(A),
x_(x),
b_(b)
{
}

template <typename Type>
matrixEq<Type>::~matrixEq()
{
}

template <typename Type>
Vector matrixEq<Type>::Solve()
{
    return Solve(1e-10);
}

template <typename Type>
Vector matrixEq<Type>::Solve(double residualTol)
{
    Vector t = A_*x_;
    Vector R1 = b_ - A_*x_;
    Vector R2 = R1;
    double p, aa, w;
    p = aa = w = 1.0;
    Vector V(b_.size());
    Vector P(b_.size());
    int Step = 0;
    while(residual() > residualTol)
    {
        double pp = p;
        p = (R2 & R1);                  //  1
        double bb = (p/pp)*(aa/w);      //  2
        P = R1 + (P - V*w)*bb;          //  3
        V = A_*P;                       //  4       
        aa = p/(R2 & V);                //  5  
        Vector S = R1 - V*aa;           //  6
        Vector T = A_*S;                //  7
        w = (T & S)/(T & T);            //  8
        x_ = x_ + P*aa + S*w;           //  9
        R1 = S - T*w;                   //  10
        Step++;
    };
    return x_;
}

template <typename Type>
double matrixEq<Type>::residual()
{
    Vector t = b_ - A_*x_;
    double r = 0;
    for (int i = 0; i < t.size(); i++) 
    {
        if (fabs(t[i]) > r) 
        {
            r = fabs(t[i]);
        }
    }
    return r;
}
