/**********************************************************************\
 * 
 * class Vector implementation
 * 
 * Description
 *      Array of double with mathematical operations
 * 
 * HeaderFiles
 *      Vector.H
 * 
\**********************************************************************/
#include "Vector.h"

using namespace std;

Vector::Vector(int n)
{
    data_ = new double[n];
    size_ = n;
    memset(data_,0.0,n*sizeof(double));
}

Vector::Vector(const Vector &v)
{
    size_ = v.size_;
    data_ = new double[size_];
    memcpy(data_,v.data_,size_*sizeof(double));
}

Vector::~Vector(void)
{
    delete [] data_;
}
/*
 * operators
*/

double& Vector::operator[] (int i)
{
    return data_[i];
}

Vector Vector::operator+ (const Vector &v)
{
    Vector t(size_);
    //if (v.size_ != size_) throw invalid_argument("size");
    for (int i = 0; i < size_; i++)
    {
        t.data_[i] = data_[i] + v.data_[i];
    }
    return t;
}
Vector Vector::operator- (const Vector &v)
{
    Vector t(size_);
    //if (v.size_ != size_) throw invalid_argument("size");
    for (int i = 0; i < size_; i++)
    {
        t.data_[i] = data_[i] - v.data_[i];
    }
    return t;
}
Vector Vector::operator* (double a)
{
    Vector t(size_);
    for (int i = 0; i < size_; i++)
    {
        t.data_[i] = data_[i]*a;
    }
    return t;
}

Vector Vector::operator/ (double a)
{
    return *this*(1.0/a);
}

Vector Vector::operator= (const Vector &v)
{
    delete [] data_;
    data_ = new double[v.size_];
    memcpy(data_,v.data_,size_*sizeof(double));
    return *this;
}
double Vector::operator& (const Vector &v)
{
    double t = 0;
    //if (v.size_ != size_) throw invalid_argument("size");
    for (int i = 0; i < size_; i++)
    {
        t += data_[i]*v.data_[i];
    }
    return t;
}

int Vector::size() 
{
    return size_;
}

