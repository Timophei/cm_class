/**********************************************************************\
 * 
 * class Vector declaration
 * 
 * Description
 *      Array of double with mathematical operations
 * 
 * SourceFiles
 *      Vector.cpp
 * 
\**********************************************************************/
#include <cstring>

using namespace std;

class Vector
{
private:

    double* data_;
    int size_;
    
public:

    Vector(int n);
    Vector(const Vector &v);
    ~Vector(void);
    
    double& operator[] (int i);         // []
    double operator& (const Vector &v); // scalar multiply
    Vector operator+ (const Vector &v); //  +
    Vector operator- (const Vector &v); //  -
    Vector operator= (const Vector &v); //  =
    Vector operator* (double a);        //  X/a
    Vector operator/ (double a);        //  aX
    
    int size();
};
