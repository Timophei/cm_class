/**********************************************************************\
 * 
 * class diagonal3Matrix declaration
 * 
 * Description
 *      square 3 diagonal maxrix with mathematical operations
 * 
 *      | M U 0   ... 0 |
 *      | L M U       . |     M - main diagonal 
 *      | 0 L M       . |     
 *      | . ... .     . |     U - upper diagonal 
 *      | . ...   M U 0 |     
 *      | . ...   L M U |     L - lower diagonal 
 *      | 0 ...   0 L M |
 * 
 * SourceFiles
 *      diagonal3Matrix.cpp
 * 
\**********************************************************************/
#include "Vector.h"

using namespace std;

class diagonal3Matrix
{
    private:

        Vector main_;
        Vector lower_;
        Vector upper_;

    public:

        diagonal3Matrix(int n);
        diagonal3Matrix(const diagonal3Matrix& m);
        diagonal3Matrix(const Vector& main, const Vector& lower, const Vector& upper);
        ~diagonal3Matrix();

        diagonal3Matrix operator= (const diagonal3Matrix& m);
        diagonal3Matrix operator+ (const diagonal3Matrix& m);
        diagonal3Matrix operator- (const diagonal3Matrix& m);
        diagonal3Matrix operator* ( diagonal3Matrix& m);
        Vector operator* ( Vector& v);
        diagonal3Matrix operator* (double a);
        diagonal3Matrix operator/ (double a);

};
