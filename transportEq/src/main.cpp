
#include "transportEq1D.h"

namespace po = boost::program_options;

using namespace std;

double Func_step(double x, double a, double y_l, double y_r)
{
    if (x < a) return y_l;
    else return y_r;
}

double analyticSolution(double x, double a, double t, double u, double g)
{
    if (x > a*t)
    {
        return u;
    }
    else return g;
    
}

int main ()
{
    cout << "Read config file" << endl;
    ifstream configFile("config.cfg");
    po::options_description desc("Allowed options");
    desc.add_options()
    ("N", po::value<int>(), "Number of nodes")
    ("xi", po::value<double>(), "Start x coord")
    ("xf", po::value<double>(), "End x coord")
    ("a", po::value<double>(), "velocity")
    ("end_t", po::value<double>(), "End time")
    ("CFL", po::value<double>(), "CFL number")
    ("mode", po::value<int>(), "0 - Explicit 1 - Implicit")
    ("Func_step_a", po::value<double>(), "Position of step func step")
    ("Func_step_y_l", po::value<double>(), "Value of step func x < a")
    ("Func_step_y_r", po::value<double>(), "Value of step func x > a");
    
    po::variables_map vm;
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
    configFile.close();
    
    transportEq1D task(vm);
    
    // ExplicitSolve
    task.InitialCondition
    (
        "eU",
        [&] (double t)
        {
            return Func_step
                (
                    t,
                    vm["Func_step_a"].as<double>(),
                    vm["Func_step_y_l"].as<double>(),
                    vm["Func_step_y_r"].as<double>()
                );
        }
    );
    task.Save();
    task.ExplicitSolve();
    task.Save();
    // ImplicitSolve
    task.InitialCondition
    (
        "iU",
        [&] (double t)
        {
            return Func_step
                (
                    t,
                    vm["Func_step_a"].as<double>(),
                    vm["Func_step_y_l"].as<double>(),
                    vm["Func_step_y_r"].as<double>()
                );
        }
    );
    task.ImplicitSolve();
    task.Save();
    
    // analyticSolution 
    int N_ = vm["N"].as<int>();
    double xi_ = vm["xi"].as<double>();
    double xf_ = vm["xf"].as<double>();
    double dx_ = (xf_-xi_)/(N_-1.0);
    double a_ = vm["a"].as<double>();
    double end_t_ = vm["end_t"].as<double>();
    ofstream s("solution.dat");
    for(int i = 0; i < N_; i++)
    {
        double x = dx_*i;
        s << x << "\t" << analyticSolution(x,a_,end_t_,2,4) << endl;
    }
    s.close();
    return 0;
}

