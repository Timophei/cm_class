/**********************************************************************\
 * 
 * class diagonal3Matrix implementation
 * 
 * Description
 *      square 3 diagonal maxrix with mathematical operations
 * 
 * HeaderFiles
 *      diagonal3Matrix.H
 * 
\**********************************************************************/

#include "diagonal3Matrix.h"

diagonal3Matrix::diagonal3Matrix(int n):
main_(n),
lower_(n-1),
upper_(n-1)
{
}

diagonal3Matrix::diagonal3Matrix(const diagonal3Matrix& m):
main_(m.main_),
lower_(m.lower_),
upper_(m.upper_)
{
}

diagonal3Matrix::diagonal3Matrix(const Vector& main, const Vector& lower, const Vector& upper):
main_(main),
lower_(lower),
upper_(upper)
{
}

diagonal3Matrix::~diagonal3Matrix()
{
}

diagonal3Matrix diagonal3Matrix::operator= (const diagonal3Matrix& m)
{
    main_ = m.main_;
    lower_ = m.lower_;
    upper_ = m.upper_;
    return *this;
}

diagonal3Matrix diagonal3Matrix::operator+ (const diagonal3Matrix& m)
{
    return diagonal3Matrix
    (
        main_ + m.main_,
        lower_ + m.lower_,
        upper_ + m.upper_
    );
}

diagonal3Matrix diagonal3Matrix::operator- (const diagonal3Matrix& m)
{
    return diagonal3Matrix
    (
        main_ - m.main_,
        lower_- m.lower_,
        upper_ - m.upper_
    );
}
diagonal3Matrix diagonal3Matrix::operator* ( diagonal3Matrix& m)
{
    int N = main_.size();
    int n = N-1;
    
    Vector tmain(N);
    Vector tlower(n);
    Vector tupper(n);
    
    for (int i = 1; i < N-1; i++)
    {
        tmain[i] = lower_[i-1]*m.upper_[i-1] + main_[i]*m.main_[i] + upper_[i]*m.lower_[i];
    }
    tmain[0] = main_[0]*m.main_[0] + m.lower_[0]*upper_[0];
    tmain[N-1] = main_[N-1]*m.main_[N-1] + lower_[n-1]*m.upper_[n-1];
    
    for (int i = 1; i < n-1; i++)
    {
        tlower[i] = lower_[i]*m.main_[i] + main_[i+1]*m.lower_[i];
    }
    
    for (int i = 0; i < n; i++)
    {
        tupper[i] = main_[i]*m.upper_[i] + upper_[i]*m.main_[i+1];
    }
    
    return diagonal3Matrix
    (
        tmain,
        tlower,
        tupper
    );
}

Vector diagonal3Matrix::operator* ( Vector& v)
{
    int n = main_.size();
    Vector t(n);
    
    for (int i = 1; i < n-1; i++)
    {
        t[i] = lower_[i-1]*v[i-1] + main_[i]*v[i] + upper_[i]*v[i+1];
    }
    t[0] = main_[0]*v[0] + upper_[0]*v[1];
    t[n-1] = lower_[n-2]*v[n-2] + main_[n-1]*v[n-1];
    
    return t;
}

diagonal3Matrix diagonal3Matrix::operator* (double a)
{
    return diagonal3Matrix
    (
        main_*a,
        lower_*a,
        upper_*a
    );
}

diagonal3Matrix diagonal3Matrix::operator/ (double a)
{
    return *this*(1.0/a);
}
