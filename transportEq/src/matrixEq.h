/**********************************************************************\
 * 
 * template class matrixEq declaration
 * 
 * Description
 *      matrix equation type Ax = b
 *      A - matrix
 *      b, x - Vectors
 *      for all marix classes with defined multiply on Vector operator*
 *      using BiCGStab method
 * 
 * SourceFiles
 *      matrixEq.cpp
 * 
\**********************************************************************/

#include "cmath"

template<typename Type> 
class matrixEq 
{
    private:
        Type A_;
        Vector b_;
        Vector x_;
        
        double residual();
    
    public:
    
        matrixEq(Type & A,const Vector& x, const Vector& b);
        ~matrixEq();
        
        Vector Solve(double residualTol);
        Vector Solve();
};

#include "matrixEq.cpp"
