#include <iostream>
#include <bitset>
#include <math.h> 

using namespace std;

union u_float
{
    float warf;
    unsigned long int wari;
}; 

union u_double
{
    double warf;
    unsigned long int wari;
}; 

int main ()
{
    float eps_f = 1.0;
    float one_f = 1.0;
    double eps_d = 1.0;
    double one_d = 1.0;
    int n_f = 0;
    int n_d = 0;
    
    cout << "float" << endl;
    do
    {
        eps_f =+ eps_f/2.0;
        n_f++;
        u_float u_eps_f;
        u_eps_f.warf = eps_f;
        cout << n_f << " " << eps_f << " " << bitset<64>(u_eps_f.wari) <<endl;
        if ((one_f + eps_f) == one_f) break;
    }
    while(true);
    cout << "double" << endl;
    do
    {
        eps_d =+ eps_d/2.0;
        n_d++;
        u_double u_eps_d;
        u_eps_d.warf = eps_d;
        cout << n_d << " " << eps_d << " " << bitset<64>(u_eps_d.wari) <<endl;
        if ((one_d + eps_d) == one_d) break;
    }
    while(true);
    cout << "int" << endl;
    int war = 1;
    int count = 1;
    
    do
    {
        war *= 2;
        count++;
        cout << count << " " << war << " " << bitset<64>(war) <<endl;
        if (war <= eps_d) break;
    }
    while(true);
    cout << "double 2" << endl;
    double warf = 1.0;
    int count1 = 1;
    do
    {
        warf *= 2.0;
        count1++;
        u_double u_war_d;
        u_war_d.warf = warf;
        cout << count1 << " " << warf << " " << bitset<64>(u_war_d.wari) <<endl;
        if (isinf(warf)) break;
    }
    while(true);
    
    return 0;
}

